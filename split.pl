#!/usr/bin/perl
use strict;
use warnings;

use JSON;

sub get_users_conf {
	my $file = shift;

	my $s = '';
	open(F, "<$file");
	foreach (<F>) {
		$s .= $_;
	}
	return decode_json($s); # croaks on error
}

sub chgrp {
	my ($user, $file) = @_;

	my ($login,$pass,$uid,$gid) = getpwnam($user);
	if (!$gid) { # use the default catch-all group for removed accounts
		$gid = 31025;
	}
	chown 0, $gid, $file;
}

my $al_f = shift @ARGV;
my $conf_dir = shift @ARGV;
my $outp_dir = shift @ARGV;

if (!defined($al_f) || !defined($conf_dir) || !defined($outp_dir)) {
	print "Usage: $0 access_log config_dir output_dir\n";
	exit;
}

print localtime().": Starting log parse\n";
my $cnt = 0;

if (!-e "$outp_dir") {
	print "ERROR: Parent output directory doesn't exist: $outp_dir\n";
	exit;
}

open(F, "<$al_f");
foreach my $line (<F>) {
	chomp($line);
	if ($line =~ /^(\S+) 0.\d+ sample Apache access line/) {
		my $vhost = $1;
		my $conf = get_users_conf("$conf_dir/users.json");
		foreach my $row (@{$conf}) {
			if ($row->{'domain'} eq $vhost) {
				my $user = $row->{'user'};
				my $log_dir = "$outp_dir/$user";
				mkdir($log_dir);
				open(F, ">>$log_dir/$vhost.log");
				print F "$line\n";
				close(F);
				chgrp($user, "$log_dir/$vhost.log"); # account into FS-quota
				++$cnt;
			}
		}
	}
}

print localtime().": Compress log and move to an archive location\n";
if (0) { # enabled in production
	system("gzip $al_f");
	system("mv $al_f /var/lib/archives/http/$al_f");
}

print localtime().": End log parse (wrote $cnt lines)\n";
exit(0);
